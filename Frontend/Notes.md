# Folder Structure
- e2e: automated tests to simulate a user
- node_modules: development libraries, won’t be deployed
- src: the actual place of source code
    - app: modules and components
    - assets: images, icons, and other resources
    - environments: production and development environments, configurationsettings for environments
    - main.ts: starting point for the application
    - polyfills.ts: if the current version is ahead of the browser support, this will handle the issue
    styles.css: global styles for the application
    tests.ts: setting up the tests
- angulas.json: the standard config settings
- editorconfig: all the developers should use the rules written in this
- karma.conf.js: running tests
- package.json: every node project's standard config file
  - dependencies: app is dependent on these 3rd party libraries(vendor)
  - devDevpenendecies: to develop applicatons
- protrator: e2e tests
- ts config: typescript related compiler settings
tsling: check typescript for readability, maintainability, structure

# Beginning of an Angular App
  main.ts -> app.module.ts -> app.component files

## Modules
  mostly we gonna need only one module, only at more complex apps we need to create more

# Components
  Our "root component": AppComponent
  What bundles our project: Webpack
  For example: Header, Main Area, Sidebar
  Key feature in Angular, to create reusable codeunits
  ## Adding a component conventions: 
  - new folder
  - newcomponent.component.ts:
    + a typescript class: NewComponent (camel case naming convention)
    + selector must be unique to avoid unexpected behaviour
    + adding a class decorator: meaning this is a component related class(selector, templateUrl or just template, one of them has to be present, styleUrl)
    + register a new component: app.module.ts: @NgModule: declarations
    + after using the <app-server></app-server> SELECTOR for inserting the server-component into the app component, we can use this as many times as we want, also we can use this in other components as well not only in AppComponent
  ## Using the CLI to create component:
  ng generate component servers OR ng g c server which does the very same
  this automatically updates the module whith the necessary "registration"
# Component Templates
  Inline and External file templates: 
    internal: We can insert html code into typescript too, not just the other way around. 
    like this(servers.component.ts): template: `
      <app-server></app-server>
      <app-server></app-server>
    `,
    external file: templateUrl: './xyz.component.html'
# Component Styles
  we can do the previoues two ways here too(external file, inline method)
  external file: styleUrls: ['./app.component.css']
  inline: styles: [`
    color: dodgerblue;
  `]
  we cant combine the two, only just one

# Selectors:
Different usages:
- TS: selector: 'app-server' --> HTML: <app-server></app-server> this is the better choice, most of the time
- TS: selector: '[app-servers]' --> HTML: <div app-servers></div>
- TS: selector: '.app-servers' --> HTML: <div class="app-servers"></div>

# DataBinding:
Communication between the TS Code(business logic) and the Template(HTML), to output data and to react to User Events. There are 4 types of it.
- String Interpolation: {{ data }}
- Property Binding: [property] = "data"
- Event binding: (event)="expression"
- Two-Way-Binding: [(ngModel)]="data" (the combination of both)

# String Interpolation:
has to be able to be converted into a string, what we write between the {{ signs }}.

# Property Binding:
used for changing properties of dom elements

# EventBinding
HTML:
  (click)="onCreateServer()"
  (input)="onUpdateServerName($event)
TS:
  onUpdateServerName(event: Event) {
    this.serverName = (<HTMLInputElement>event.target).value;
  }

# Two-Way-DataBinding
combination of event and property binding
HTML:
  <input type="text" class="form-control" [(ngModel)]="serverName">
TS:
  serverName = 'Example'
  onUpdateServerName(event: Event) {
    this.serverName = (<HTMLInputElement>event.target).value;
  }

# Directives


--------------------------------------------------------------------------
# Tour of Heroes
ng serve --open -> egybol kinyitja a bongeszoben

# Basic Concepts
- Components: Components are the fundamental building blocks of Angular applications. They display data on the screen, listen for user input, and take action based on that input.
- NgModules: provides a compilation context for components.
- Decorators: Modules, components and services are classes that use decorators. These decorators mark their type and provide metadata that tells Angular how to use them.

- UppercasePipe: good way to format strings, currency amounts, dates and other display data

- For loop:
<ul class="heroes">
  <li *ngFor='let hero of heroes'>
    <span class="badge">{{hero.id}}</span> {{hero.name}}
  </li>
</ul>

- Input decorator: to make the hero property available for binding by the external HeroesComponent in the HeroeDetailComponent

- Service: Components shouldn't fetch or save data directly and they certainly shouldn't knowingly present fake data. They should focus on presenting data and delegate data access to a service.
We can remove it easily, if we decide later that we do not need it anymore.

- Observable: makes it easier to compose asynchronous or callback-based code
  - of(): returns an Observable<Hero[]> that emits a single value, the array of mock heroes
  - Observable.subscribe(): 

# Angular only binds to public component properties.

Routing:
  ng generate module app-routing --flat --module=app
  --flat puts the file in src/app instead of its own folder.
  --module=app tells the CLI to register it in the imports array of the AppModule.

  imports: [RouterModule.forRoot(routes)] adds the RouterModule to the AppRoutingModule imports array and configures it with the routes in one step
  exports: [RouterModule] exports RouterModule so it will be available throughout the app

  +this.route.snapshot.paramMap.get('id') --> + sign converts the string to a number, which is what a hero id should be.
