<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\JsonResponse;
use App\Hero;
use Tests\TestCase;

class HeroTest extends TestCase
{
    use RefreshDatabase;

    public function testGet()
    {
        $hero1 = factory(Hero::class)->create();
        $hero2 = factory(Hero::class)->create();

        $response = $this->getJson('/api/heroes')
            ->assertStatus(200);
        $response->assertJsonStructure(['status', 'messages', 'data']);
        $response->assertJson([
            "status" => "success",
            "messages" => [],
            "data" => Hero::all()->toArray()
        ]);
    }

    public function testGetwithid()
    {
        $hero = factory(Hero::class)->create();
        $response = $this->getJson('/api/heroes/' . $hero->id)
            ->assertStatus(200);

        $response->assertJsonStructure(['status', 'messages', 'data']);
        $response->assertJson([
            "status" => "success",
            "messages" => [],
            "data" => $hero->toArray()
        ]);
    }

    public function testPost()
    {
        $hero = factory(Hero::class)->make();
        // $hero = factory(Hero::class)->make();

        $response = $this->postJson('/api/heroes', $hero->toArray())
            ->assertStatus(201);
        $response->assertJsonStructure(['status', 'messages', 'data']);
        $response->assertJson([
            "status" => "success",
            "messages" => [],
            "data" => $hero->toArray()
        ]);
        $this->assertDatabaseHas('heroes', $hero->toArray());
    }

    public function testPut()
    {
        $hero1 = factory(Hero::class)->create();
        $hero2 = factory(Hero::class)->make(); // nem jo create-el?

        $response = $this->putJson('/api/heroes/' . $hero1->id, $hero2->toArray())
            ->assertStatus(200);
        $response->assertJsonStructure(['status', 'messages', 'data']);
        $response->assertJson([
            "status" => "success",
            "messages" => [],
            "data" => $hero2->toArray(),
        ]);
        $this->assertDatabaseHas('heroes', $hero2->toArray());
    }

    public function testDelete()
    {
        $hero = factory(Hero::class)->create([
            'name' => 'Marvel',
        ]);

        $response = $this->deleteJson('/api/heroes/' . $hero->id)
            ->assertStatus(200);
        $response->assertJsonStructure(['status', 'messages', 'data']);
        $response->assertJson([
            "status" => "success",
            "messages" => [],
            "data" => $hero->toArray(),
        ]);
        $this->assertDatabaseMissing('heroes', $hero->toArray());
    }
}
