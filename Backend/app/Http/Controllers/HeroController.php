<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Response;
use App\Http\Controllers\Exception;
use Illuminate\Http\Request;
use App\Hero;

class HeroController extends Controller
{
    public function index()
    {
        try {
            $isThereAnyHero = Hero::firstOrFail();
            $heroes = Hero::all();
            return response()->json([
                'status' => 'success',
                'messages' => [],
                'data' => $heroes->toArray()
            ],
            200);
        } catch(\Exception $e) {
            // \Log::debug('Hero was not found!');
            return response()->json([
                'status' => 'error',
                'messages' => ['There\'s no hero yet!'],
                'data' => []
            ],
            404);
        }
    }
 
    public function show($id)
    {
        try {
            $hero = Hero::findOrFail($id);
            return response()->json([
                'status' => 'success',
                'messages' => [],
                'data' => $hero->toArray()
            ],
            200);
        } catch(\Exception $e) {
            // \Log::debug('Hero was not found!');
            return response()->json([
                'status' => 'error',
                'messages' => ['Hero not found!'],
                'data' => []
            ],
            404);
        }
    }

    public function store(Request $request)
    {
        $hero = Hero::create($request->all());

        return response()->json([
            'status' => 'success',
            'messages' => [],
            'data' => $hero->toArray()
        ],
        201);
    }

    public function update(Request $request, $id)
    {
        try {
            $hero = Hero::findOrFail($id);
            $hero->update($request->all());
    
            return response()->json([
                'status' => 'success',
                'messages' => [],
                'data' => $hero->toArray()
            ],
            200);
        } catch(\Exception $e) {
            // \Log::debug('Hero was not found!');
            return response()->json([
                'status' => 'error',
                'messages' => ['Hero not found!'],
                'data' => []
            ],
            404);
        }
    }

    public function delete(Request $request, $id)
    {
        try {
            $hero = Hero::findOrFail($id);
            $hero->delete();
    
            return response()->json([
                'status' => 'success',
                'messages' => [],
                'data' => $hero->toArray()
            ],
            200);
        } catch(\Exception $e) {
            // \Log::debug('Hero was not found!');
            return response()->json([
                'status' => 'error',
                'messages' => ['Hero not found!'],
                'data' => []
            ],
            404);
        }

    }
}
