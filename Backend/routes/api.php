<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Hero;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('heroes', 'HeroController@index');
Route::get('heroes/{id}', 'HeroController@show');
Route::post('heroes', 'HeroController@store');
Route::put('heroes/{id}', 'HeroController@update');
Route::delete('heroes/{id}', 'HeroController@delete');